<?php

namespace App\ParamConverter;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

final class UserConverter implements ParamConverterInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer,
    ) {
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        if (Request::METHOD_POST === $request->getMethod()) {
            $request->attributes->set(
                $configuration->getName(),
                $this->serializer->deserialize(
                    $request->getContent(),
                    User::class,
                    'json',
                    [AbstractNormalizer::IGNORED_ATTRIBUTES => ['customer_id']],
                ),
            );

            return true;
        }

        return false;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return User::class === $configuration->getClass();
    }
}
