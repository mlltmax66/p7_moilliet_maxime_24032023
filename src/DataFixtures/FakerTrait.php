<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;

trait FakerTrait
{
    private ?Generator $faker = null;

    public function faker(): Generator
    {
        if ($this->faker === null) {
            $this->faker = Factory::create('fr_FR');
        }

        return $this->faker;
    }
}
