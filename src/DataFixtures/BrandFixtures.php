<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class BrandFixtures extends Fixture
{
    use FakerTrait;

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 10; ++$i) {
            /** @var string $name */
            $name = $this->faker()->words(3, true);

            $manager->persist(
                (new Brand())->setName($name)
            );
        }

        $manager->flush();
    }
}
