<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class CustomerFixtures extends Fixture
{
    use FakerTrait;

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 10; ++$i) {
            /** @var string $name */
            $name = $this->faker()->words(3, true);

            $manager->persist(
                (new Customer())
                    ->setName($name)
                    ->setEmail($this->faker()->email)
                    ->setPhone($this->faker()->phoneNumber)
                    ->setCity($this->faker()->city)
                    ->setCountry($this->faker()->countryCode)
            );
        }

        $manager->flush();
    }
}
