<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserFixtures extends Fixture
{
    use FakerTrait;

    public function __construct(private readonly UserPasswordHasherInterface $hasher)
    {
    }

    /**
     * @return array<array-key, class-string<Fixture>>
     */
    public function getDependencies(): array
    {
        return [CustomerFixtures::class];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var array<array-key, Customer> $customers */
        $customers = $manager->getRepository(Customer::class)->findAll();

        foreach ($customers as $key => $customer) {
            for ($i = 1; $i <= 10; ++$i) {
                /** @var string $name */
                $name = $this->faker()->words(3, true);

                $user = (new User())
                    ->setName($name)
                    ->setEmail($this->faker()->email)
                    ->setCustomer($customer);

                if (1 === $i && 0 === $key) {
                    $user->setRoles(['ROLE_ADMIN']);
                }

                $manager->persist(
                    $user->setPassword($this->hasher->hashPassword($user, 'password'))
                );
            }
        }

        $manager->flush();
    }
}
