<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class ProductFixtures extends Fixture
{
    use FakerTrait;

    /**
     * @return array<array-key, class-string<Fixture>>
     */
    public function getDependencies(): array
    {
        return [BrandFixtures::class];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var array<array-key, Brand> $brands */
        $brands = $manager->getRepository(Brand::class)->findAll();

        foreach ($brands as $brand) {
            for ($i = 1; $i <= 20; ++$i) {
                /** @var string $name */
                $name = $this->faker()->words(3, true);
                /** @var string $description */
                $description = $this->faker()->paragraphs(3, true);

                $manager->persist(
                    (new Product())
                        ->setName($name)
                        ->setDescription($description)
                        ->setPrice(rand(1000, 50000))
                        ->setStock(rand(10, 1000))
                        ->setYearManufacture(rand(2010, 2023))
                        ->setBrand($brand)
                );
            }
        }

        $manager->flush();
    }
}
