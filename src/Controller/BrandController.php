<?php

namespace App\Controller;

use App\Entity\Brand;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[OA\Tag('Brands')]
#[Route('/brands', name: 'brand_')]
final class BrandController extends AbstractController
{
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return Brand item',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'id',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'name',
                    type: 'string',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                    )
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return Brand item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 401
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/{id}', name: 'get_item', methods: [Request::METHOD_GET])]
    public function getItem(Brand $brand): Response
    {
        return $this->json(
            $brand,
            Response::HTTP_OK,
            ['content-type' => 'application/hal+json']
        );
    }
}
