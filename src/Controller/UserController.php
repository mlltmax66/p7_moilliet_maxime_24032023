<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\User;
use App\Hateoas\CollectionFactoryInterface;
use App\Repository\CustomerRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Attributes as OA;

#[OA\Tag('Users')]
#[Route('/users', name: 'user_')]
final class UserController extends AbstractController
{
    #[OA\RequestBody(
        request: 'User',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'name',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'email',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'password',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'customer_id',
                    type: 'integer',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Return User item',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'id',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'name',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'email',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'createdAt',
                    type: 'datetime',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'delete',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'customer',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                    )
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return Customer item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNPROCESSABLE_ENTITY,
        description: 'Unprocessable entity',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 422
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'violations',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'propertyPath',
                                type: 'string',
                            ),
                            new OA\Property(
                                property: 'message',
                                type: 'string',
                            ),
                        ],
                    )
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 401
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/', name: 'post_item', methods: [Request::METHOD_POST])]
    public function post_item(
        User $user,
        Request $request,
        CustomerRepository $customerRepository,
        EntityManagerInterface $manager,
        ValidatorInterface $validator,
        UserPasswordHasherInterface $hasher
    ): Response {
        $body = json_decode($request->getContent(), true);

        $violations = $validator->validate($user, null, ['user:write']);

        if ($violations->count() > 0) {
            throw new ValidationFailedException($user, $violations);
        }

        $user->setPassword($hasher->hashPassword($user, $body['password']));

        $customer = $customerRepository->find($body['customer_id']);

        if ($customer === null) {
            throw new NotFoundHttpException('Customer with id '.$body['customer_id'].' not found');
        }

        $user->setCustomer($customer);

        $manager->persist($user);
        $manager->flush();

        return $this->json(
            $user,
            Response::HTTP_CREATED,
            ['content-type' => 'application/hal+json']
        );
    }

    #[OA\Parameter(
        name: 'page',
        description: 'The page number',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: 'limit',
        description: 'The number of users per page',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return User collection',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'page',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'pages',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'limit',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'count',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'total',
                    type: 'integer',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'first',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'next',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'previous',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'last',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'post',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                        type: 'object',
                    )
                ),
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'id',
                                type: 'integer',
                            ),
                            new OA\Property(
                                property: 'name',
                                type: 'string',
                            ),
                            new OA\Property(
                                property: 'email',
                                type: 'string',
                            ),
                            new OA\Property(
                                property: 'createdAt',
                                type: 'datetime',
                            ),
                            new OA\Property(
                                property: '_links',
                                type: 'array',
                                items: new OA\Items(
                                    properties: [
                                        new OA\Property(
                                            property: 'self',
                                            properties: [
                                                new OA\Property(
                                                    property: 'href',
                                                    type: 'string',
                                                ),
                                            ],
                                            type: 'object',
                                        ),
                                        new OA\Property(
                                            property: 'delete',
                                            properties: [
                                                new OA\Property(
                                                    property: 'href',
                                                    type: 'string',
                                                ),
                                            ],
                                            type: 'object',
                                        ),
                                        new OA\Property(
                                            property: 'customer',
                                            properties: [
                                                new OA\Property(
                                                    property: 'href',
                                                    type: 'string',
                                                ),
                                            ],
                                            type: 'object',
                                        ),
                                    ],
                                    type: 'object',
                                )
                            ),
                        ],
                        type: 'object'
                    ),
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return Customer item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/{id}', name: 'get_collection', methods: [Request::METHOD_GET])]
    public function getCollection(
        Customer $customer,
        Request $request,
        UserRepository $userRepository,
        CollectionFactoryInterface $collectionFactory
    ): Response {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);

        $users = $userRepository->findBy(
            ['customer' => $customer],
            ['id' => 'asc'],
            $limit,
            ($page - 1) * $limit
        );

        return $this->json(
            $collectionFactory->create(
                'users',
                $users,
                $page,
                $limit,
                $userRepository->count(['customer' => $customer]),
                'user_get_collection',
                ['id' => $customer->getId()]
            )->addLinks(
                'post',
                $this->generateUrl(
                    'user_post_item',
                    [],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ),
            Response::HTTP_OK,
            ['content-type' => 'application/hal+json'],
        );
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return User item',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'id',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'name',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'email',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'createdAt',
                    type: 'datetime',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'delete',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'customer',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                    )
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return User item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 401
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/{id}/detail', name: 'get_item', methods: [Request::METHOD_GET])]
    public function getItem(User $user): Response
    {
        return $this->json(
            $user,
            Response::HTTP_OK,
            ['content-type' => 'application/hal+json']
        );
    }

    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return User item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NO_CONTENT,
        description: 'No content'
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 401
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/{id}/delete', name: 'remove_item', methods: [Request::METHOD_DELETE])]
    public function removeItem(User $user, EntityManagerInterface $manager): Response
    {
        $manager->remove($user);
        $manager->flush();

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
