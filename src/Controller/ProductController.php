<?php

namespace App\Controller;

use App\Entity\Product;
use App\Hateoas\CollectionFactoryInterface;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[OA\Tag('Products')]
#[Route('/products', name: 'product_')]
final class ProductController extends AbstractController
{
    #[OA\Parameter(
        name: 'page',
        description: 'The page number',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: 'limit',
        description: 'The number of products per page',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return Product collection',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'page',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'pages',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'limit',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'count',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'total',
                    type: 'integer',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'first',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'next',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'previous',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'last',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                        type: 'object',
                    )
                ),
                new OA\Property(
                    property: 'data',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'id',
                                type: 'integer',
                            ),
                            new OA\Property(
                                property: 'name',
                                type: 'string',
                            ),
                            new OA\Property(
                                property: 'description',
                                type: 'string',
                            ),
                            new OA\Property(
                                property: 'price',
                                type: 'integer',
                            ),
                            new OA\Property(
                                property: 'stock',
                                type: 'integer',
                            ),
                            new OA\Property(
                                property: 'year_manufacture',
                                type: 'integer',
                            ),
                            new OA\Property(
                                property: 'createdAt',
                                type: 'datetime',
                            ),
                            new OA\Property(
                                property: '_links',
                                type: 'array',
                                items: new OA\Items(
                                    properties: [
                                        new OA\Property(
                                            property: 'self',
                                            properties: [
                                                new OA\Property(
                                                    property: 'href',
                                                    type: 'string',
                                                ),
                                            ],
                                            type: 'object',
                                        ),
                                        new OA\Property(
                                            property: 'brand',
                                            properties: [
                                                new OA\Property(
                                                    property: 'href',
                                                    type: 'string',
                                                ),
                                            ],
                                            type: 'object',
                                        ),
                                    ],
                                    type: 'object',
                                )
                            ),
                        ],
                        type: 'object'
                    ),
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/', name: 'get_collection', methods: [Request::METHOD_GET])]
    public function getCollection(
        Request $request,
        CollectionFactoryInterface $collectionFactory,
        ProductRepository $productRepository
    ): Response {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);

        $products = $productRepository->findBy([], ['name' => 'asc'], $limit, ($page - 1) * $limit);

        return $this->json(
            $collectionFactory->create(
                'products',
                $products,
                $page,
                $limit,
                $productRepository->count([]),
                'product_get_collection',
            ),
            Response::HTTP_OK,
            ['content-type' => 'application/hal+json'],
        );
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Return Product item',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'id',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'name',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'description',
                    type: 'string',
                ),
                new OA\Property(
                    property: 'price',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'stock',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'year_manufacture',
                    type: 'integer',
                ),
                new OA\Property(
                    property: 'createdAt',
                    type: 'datetime',
                ),
                new OA\Property(
                    property: '_links',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(
                                property: 'self',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                            new OA\Property(
                                property: 'brand',
                                properties: [
                                    new OA\Property(
                                        property: 'href',
                                        type: 'string',
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                    )
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Return Product item not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 404
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[OA\Response(
        response: Response::HTTP_UNAUTHORIZED,
        description: 'Unauthorized',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(
                    property: 'code',
                    type: 'integer',
                    example: 401
                ),
                new OA\Property(
                    property: 'message',
                    type: 'string',
                ),
            ],
            type: 'object'
        ),
    )]
    #[Route('/{id}', name: 'get_item', methods: [Request::METHOD_GET])]
    public function getItem(Product $product): Response
    {
        return $this->json(
            $product,
            Response::HTTP_OK,
            ['content-type' => 'application/hal+json']
        );
    }
}
