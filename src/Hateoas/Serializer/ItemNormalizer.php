<?php

namespace App\Hateoas\Serializer;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

abstract class ItemNormalizer implements NormalizerInterface
{
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @return string[]
     */
    abstract protected function getContext(): array;

    /**
     * @return array<string, array{href: string}>
     */
    abstract protected function getLinks(mixed $object): array;

    /**
     * @return array<string, mixed>
     *
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $normalizeData = $this->normalizer->normalize($object, $format, ['groups' => $this->getContext()]);

        return $normalizeData + ['_links' => $this->getLinks($object)];
    }
}
