<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
class Customer
{
    #[Groups('customer:read')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[Groups('customer:read')]
    #[ORM\Column(type: 'string')]
    private string $name;

    #[Groups('customer:read')]
    #[ORM\Column(type: 'string')]
    private string $email;

    #[Groups('customer:read')]
    #[ORM\Column(type: 'string')]
    private string $phone;

    #[Groups('customer:read')]
    #[ORM\Column(type: 'string')]
    private string $city;

    #[Groups('customer:read')]
    #[ORM\Column(type: 'string')]
    private string $country;

    /**
     * @var Collection<int, User>
     */
    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: User::class)]
    private Collection $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection<int, User> $users
     *
     * @return $this
     */
    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }
}
