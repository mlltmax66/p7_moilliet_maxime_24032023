<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[Groups('product:read')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[Groups('product:read')]
    #[ORM\Column(type: 'string')]
    private string $name;

    #[Groups('product:read')]
    #[ORM\Column(type: 'text')]
    private string $description;

    #[Groups('product:read')]
    #[ORM\Column(type: 'integer')]
    private int $price;

    #[Groups('product:read')]
    #[ORM\Column(type: 'integer')]
    private int $stock;

    #[Groups('product:read')]
    #[ORM\Column(type: 'integer')]
    private int $year_manufacture;

    #[Groups('product:read')]
    #[ORM\Column(type: 'date_immutable')]
    private \DateTimeImmutable $createdAt;

    #[ORM\ManyToOne(targetEntity: Brand::class, inversedBy: 'products')]
    private Brand $brand;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getYearManufacture(): int
    {
        return $this->year_manufacture;
    }

    public function setYearManufacture(int $year_manufacture): self
    {
        $this->year_manufacture = $year_manufacture;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getBrand(): Brand
    {
        return $this->brand;
    }

    public function setBrand(mixed $brand): self
    {
        $this->brand = $brand;

        return $this;
    }
}
