<?php

namespace App\Serializer;

use App\Entity\Product;
use App\Hateoas\Serializer\ItemNormalizer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class ProductNormalizer extends ItemNormalizer
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator, ObjectNormalizer $normalizer)
    {
        parent::__construct($normalizer);
    }

    protected function getContext(): array
    {
        return ['product:read'];
    }

    protected function getLinks(mixed $object): array
    {
        return [
            'self' => [
                'href' => $this->urlGenerator->generate(
                    'product_get_item',
                    ['id' => $object->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
            'brand' => [
                'href' => $this->urlGenerator->generate(
                    'brand_get_item',
                    ['id' => $object->getBrand()->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof Product;
    }
}
