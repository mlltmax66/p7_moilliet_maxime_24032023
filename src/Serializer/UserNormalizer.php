<?php

namespace App\Serializer;

use App\Entity\User;
use App\Hateoas\Serializer\ItemNormalizer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class UserNormalizer extends ItemNormalizer
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator, ObjectNormalizer $normalizer)
    {
        parent::__construct($normalizer);
    }

    protected function getContext(): array
    {
        return ['user:read'];
    }

    protected function getLinks(mixed $object): array
    {
        return [
            'self' => [
                'href' => $this->urlGenerator->generate(
                    'user_get_item',
                    ['id' => $object->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
            'delete' => [
              'href' => $this->urlGenerator->generate(
                  'user_remove_item',
                  ['id' => $object->getId()],
                  UrlGeneratorInterface::ABSOLUTE_URL
              ),
            ],
            'customer' => [
                'href' => $this->urlGenerator->generate(
                    'customer_get_item',
                    ['id' => $object->getCustomer()->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof User;
    }
}
