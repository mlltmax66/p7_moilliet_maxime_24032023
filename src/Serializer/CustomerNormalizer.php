<?php

namespace App\Serializer;

use App\Entity\Customer;
use App\Hateoas\Serializer\ItemNormalizer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class CustomerNormalizer extends ItemNormalizer
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator, ObjectNormalizer $normalizer)
    {
        parent::__construct($normalizer);
    }

    protected function getContext(): array
    {
        return ['customer:read'];
    }

    protected function getLinks(mixed $object): array
    {
        return [
            'self' => [
                'href' => $this->urlGenerator->generate(
                    'customer_get_item',
                    ['id' => $object->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof Customer;
    }
}
