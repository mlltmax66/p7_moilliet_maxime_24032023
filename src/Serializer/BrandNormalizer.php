<?php

namespace App\Serializer;

use App\Entity\Brand;
use App\Hateoas\Serializer\ItemNormalizer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class BrandNormalizer extends ItemNormalizer
{
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator, ObjectNormalizer $normalizer)
    {
        parent::__construct($normalizer);
    }

    protected function getContext(): array
    {
        return ['brand:read'];
    }

    protected function getLinks(mixed $object): array
    {
        return [
            'self' => [
                'href' => $this->urlGenerator->generate(
                    'brand_get_item',
                    ['id' => $object->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof Brand;
    }
}
