# BileMo

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ed9c0cca898342e8bb8f9abf9a634850)](https://app.codacy.com/gl/mlltmax66/p7_moilliet_maxime_24032023/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

Création d'une API pour une entreprise (BileMo) offrant toute une sélection de téléphones mobiles haut de gamme.

## Prérequis

PHP version >= 8.1 et composer

## Installation

1. Clonez ou téléchargez le repository :
```
    git clone https://gitlab.com/mlltmax66/p7_moilliet_maxime_24032023
```

2. Configurez vos variables d'environnement tel que la connexion à la base de données dans le fichier `.env` :

3. Installez les dépendances du projet avec [Composer](https://getcomposer.org/download/) :
```
    composer install
```

4. Créez la base de données si elle n'existe pas déjà :
```
    php bin/console d:d:c
```

5. Ajoutez l'ensemble des tables :
```
    php bin/console d:s:u --force
```

6. (Optionnel) Lancez les fixtures :
```
    php bin/console d:f:l
```

7. Générez les clés pour activer l'authentification (JWT) :
```
    php bin/console lexik:jwt:generate-keypair
```

8. Lancement du serveur :
```
    php bin/console server:run ou php -S localhost:8000 -t public
```

9. Dirigez vous sur http://localhost:8000/api/docs. pour accéder à l'api doc.

10. Utilisez postman ou insomnia pour vous connecter à l'aide des informations de l'utilisateur : 
```
    post : http://localhost:8000/api/login
    body : {"email": "", "password": "password"}
    response : {"token": ""}
```
